package com.eairlv.utils.config;


import com.eairlv.utils.utils.HttpUtil;
import com.eairlv.utils.validator.url.UrlValidator;
import com.eairlv.utils.validator.user.UserInformation;
import com.eairlv.utils.validator.user.UserValidator;
import feign.RequestInterceptor;
import feign.RequestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * @author zhoujiwei
 * @create 2018-08-08 11:32
 * @desc
 **/
public class FeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate template) {
        UserInformation userInfo = MSInterceptor.USER_INFORMATION.get();
        if (userInfo != null) {
            template.header(UserValidator.USER_INFORMATION, HttpUtil.writeUserInfoToHeaderString(userInfo));
            return;
        }

        HttpServletRequest request = MSInterceptor.REQUEST_INFORMATION.get();
        if (null != request) {
            String jwt = request.getHeader(UrlValidator.GATEWAY_AUTHORIZATION);
            if (null != jwt && jwt.length() > 0) {
                template.header(UrlValidator.GATEWAY_AUTHORIZATION, jwt);
            }
        }
    }

}
