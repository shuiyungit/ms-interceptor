package com.eairlv.utils.support.hystrix;

import com.alibaba.ttl.TransmittableThreadLocal;

/**
 * @author tianye
 * @date 2020-10-23 16:29
 */
public class CleanDataTransmittableThreadLocal<T> extends TransmittableThreadLocal<T> {

    @Override
    protected void afterExecute() {
        super.afterExecute();
        remove();
    }
}
