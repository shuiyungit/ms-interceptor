package com.eairlv.utils.utils;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author eairlv
 * @description
 * @date 10:23 2019/6/6
 */
public class LogUtil {

    /**
     * 获取错误堆栈信息
     * @param e 异常信息
     * @return 异常信息
     */
    public static String getExceptionMessage(Exception e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }
}
