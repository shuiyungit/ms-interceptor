package com.eairlv.utils.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cglib.beans.BeanCopier;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lv
 * @create 2018-07-23 11:25
 * @desc
 **/
@Slf4j
public class BeanUtil {

    private static final Map<String, BeanCopier> BEAN_COPIER_MAP = new HashMap<>();

    /**
     * @param source 资源类
     * @param target 目标类
     * @Title: copyProperties
     */
    public static void copyProperties(Object source, Object target) {
        String beanKey = generateKey(source.getClass(), target.getClass());
        BeanCopier copier;

        if (!BEAN_COPIER_MAP.containsKey(beanKey)) {
            copier = BeanCopier.create(source.getClass(), target.getClass(), false);
            BEAN_COPIER_MAP.put(beanKey, copier);
        } else {
            copier = BEAN_COPIER_MAP.get(beanKey);
        }

        copier.copy(source, target, null);
    }

    private static String generateKey(Class<?> class1, Class<?> class2) {
        return class1.toString() + class2.toString();
    }

    /**
     * 复制Bean当中的相同名称的属性
     *
     * @param source 源
     * @param target 目标
     * @param <T>    类型
     * @return
     */
    public static <T> T copyProperties(Object source, Class<T> target) {
        if (source == null){
            return null;
        }
        T orig = null;
        try {
            orig = target.newInstance();
            copyProperties(source, orig);
        } catch (InstantiationException | IllegalAccessException e) {
            log.warn("BeanUtil copyProperties error");
        }
        return orig;
    }

    /**
     * 复制Bean当中的相同名称的属性
     * @param source
     * @param target
     * @param <T>
     * @return
     */
    public static <T> List<T> copyProperties(Collection<?> source, Class<T> target){
        return source.stream().parallel().map(e -> copyProperties(e, target)).collect(Collectors.toList());
    }
}
