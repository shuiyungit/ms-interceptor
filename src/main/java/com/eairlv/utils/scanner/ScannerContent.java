package com.eairlv.utils.scanner;

import lombok.Builder;
import lombok.Data;

import java.lang.reflect.Method;

/**
 * @author eairlv
 * @description
 * @date 15:39 2019/8/29
 */
@Data
@Builder
public class ScannerContent {

    Method interceptor;

    Method resourceHandler;

    Method argumentResolver;
}
