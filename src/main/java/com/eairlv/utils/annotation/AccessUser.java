package com.eairlv.utils.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *
 * @author eairlv
 * @description
 * @date 15:32 2019/8/16
 *
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Component
public @interface AccessUser {

    /**
     * 用户信息校验
     */
    boolean user() default true;
}
