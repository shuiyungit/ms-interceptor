package com.eairlv.utils.annotation;

import com.eairlv.utils.MSWebMvcConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author shuiyun
 * @program ms-interceptor
 * @description 应用拦截器
 * @date 2022-05-24 14:56
 **/
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({MSWebMvcConfiguration.class})
public @interface EnableInterceptor {
}
