package com.eairlv.utils.validator.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author eairlv
 * @description
 * @date 16:39 2019/8/29
 */
@Data
public class UserInformation {

    /**
     * 主键
     */
    private String userId;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号", hidden = true)
    private String userName;

    /**
     * 机构ID
     */
    private Integer orgId;

    /**
     * 访问时间
     */
    private Long time;

}
