package com.eairlv.utils.validator.gateway;

import com.eairlv.utils.annotation.AccessPermission;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;

/**
 * @author eairlv
 * @description
 * @date 11:26 2019/8/27
 */
public class GatewayValidator {

    public static final String GATEWAY = "Gateway";

    /**
     * 网关（内外部接口）验证器
     *
     * @param request
     * @param handler
     * @return
     */
    public static Boolean valid(HttpServletRequest request, HandlerMethod handler){
        if (request.getHeader(GATEWAY) != null){
            AccessPermission classAnnotation = handler.getClass().getAnnotation(AccessPermission.class);
            AccessPermission methodAnnotation = handler.getMethodAnnotation(AccessPermission.class);
            if (classAnnotation == null && methodAnnotation == null){
                return Boolean.FALSE;
            }
        } else {
            // fixme 目前仅拦截外部请求，内部请求访问外部接口默认允许，方便调试
        }
        return Boolean.TRUE;
    }
}
