package com.eairlv.utils.validator.url;

import lombok.Builder;
import lombok.Data;

/**
 * @author eairlv
 * @description
 * @date 17:33 2019/8/29
 */
@Data
@Builder
public class UrlBO {

    private String method;

    private String uri;

    private String jwt;
}
