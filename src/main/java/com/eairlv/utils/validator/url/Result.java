package com.eairlv.utils.validator.url;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author lv
 * @create 2018-07-23 10:17
 * @desc
 **/
@Data
@Builder
@Slf4j
@NoArgsConstructor
@AllArgsConstructor
public class Result<T> {

    private int code;

    private String message;

    private T data;

    /**
     * 请求成功
     *
     * @return
     */
    public static Result ok() {
        return result(HttpResult.SUCCESS.getCode(), HttpResult.SUCCESS.getMessage(), null);
    }

    public static <T> Result<T> ok(T data) {
        return result(HttpResult.SUCCESS, data);
    }

    /**
     * 请求失败
     *
     * @return
     */
    public static Result fail() {
        return result(HttpResult.FAIL.getCode(), HttpResult.FAIL.getMessage(), null);
    }

    public static Result fail(String message) {
        return result(HttpResult.FAIL.getCode(), message, null);
    }

    public static Result fail(String message, HttpResult code) {
        return result(code.getCode(), message, null);
    }

    public static <T> Result<T> result(HttpResult httpResult, T data) {
        return result(httpResult.getCode(), httpResult.getMessage(), data);
    }

    public static <T> Result<T> result(Integer code, String message, T data) {
        return Result.<T>builder()
                .code(code)
                .message(message)
                .data(data)
                .build();
    }
}
