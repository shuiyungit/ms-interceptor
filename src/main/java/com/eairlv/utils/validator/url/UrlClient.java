package com.eairlv.utils.validator.url;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.eairlv.utils.MSInterceptorProperties;
import com.eairlv.utils.validator.user.UserInformation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

/**
 * @author eairlv
 * @description
 * @date 16:36 2019/8/29
 */
@Slf4j
public class UrlClient {

    /**
     * 用户身份校验
     * @param restTemplate
     * @param properties
     * @return
     */
    public static UserInformation jwt(String method, String uri, String jwt, RestTemplate restTemplate, MSInterceptorProperties properties){
        if (StrUtil.isNotBlank(properties.getAuthenticationUrl())){
            //指定header
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization",jwt);
            HttpEntity<String> httpEntity = new HttpEntity<>( headers);
            ResponseEntity<String> exchange = restTemplate.exchange(properties.getAuthenticationUrl(), HttpMethod.GET, httpEntity, String.class);

            Result<UserInformation> userInfoResult = JSON.parseObject(exchange.getBody(),new TypeReference<Result<UserInformation>>() {});
            if (ObjectUtil.isNotNull(userInfoResult) && userInfoResult.getCode() == HttpResult.SUCCESS.getCode()){
                return userInfoResult.getData();
            }
        } else {
            log.error("ms-interceptor.authenticationURL is empty, please configure auth path Or set ms-interceptor.authentication=false");
        }
        return null;
    }

}
