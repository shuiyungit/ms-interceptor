package com.eairlv.utils.validator.url;

import lombok.Getter;
import lombok.Setter;

/**
 * @author lv
 * @create 2018-07-23 10:24
 * @desc
 **/
public enum HttpResult {

    // 请求状态
    SUCCESS("请求成功", 0),
    FAIL("请求失败", -1),
    AUTH_FAIL("不合法的token", 40001),
    INNER_INTERFACE("拒绝访问内部接口", 40005),
    NO_USER_INFO("token中缺失用户信息", 40006);

    @Setter
    @Getter
    private int code;

    @Setter
    @Getter
    private String message;

    HttpResult(String message, int code) {
        this.code = code;
        this.message = message;
    }
}
