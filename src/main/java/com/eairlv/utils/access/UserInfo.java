package com.eairlv.utils.access;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author eairlv
 * @description
 * @date 14:04 2019/4/28
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserInfo {

    /**
     * 主键
     */
    @ApiModelProperty(value = "主键", hidden = true)
    private String userId;

    /**
     * 机构ID
     */
    @ApiModelProperty(value = "机构ID", hidden = true)
    private Integer orgId;

    /**
     * 账号
     */
    @ApiModelProperty(value = "账号", hidden = true)
    private String userName;

    /**
     * 访问时间
     */
    @ApiModelProperty(value = "访问时间", hidden = true)
    private Long time;
}
