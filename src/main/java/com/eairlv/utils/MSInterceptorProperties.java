package com.eairlv.utils;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

/**
 * @author eairlv
 * @description
 * @date 15:51 2019/8/16
 *
 * {@link com.eairlv.utils.config.MSInterceptor}拦截器特殊说明
 */
@Data
@ConfigurationProperties(MSInterceptorProperties.MS_INTERCEPTOR)
public class MSInterceptorProperties {

    public static final String MS_INTERCEPTOR = "ms-interceptor";

    /**
     * 微服务拦截器总开关
     */
    private Boolean enabled = true;

    /**
     * 过滤器级别拒接访问，多个以逗号分开，支持{@link AntPathMatcher}
     * 配置拒接访问的接口，用于严格的线上环境，如需要配置拦截swagger与actuator监控等："/v2/api-docs,/actuator/**"
     */
    private String refuse = "";

    /**
     * 是否只允许{@link HandlerMethod}，类型的请求，如果为true，则非HandlerMethod类型的请求会被拒绝，如/doc.html类型{@link ResourceHttpRequestHandler};
     * 如果为false，则默认允许访问，非HandlerMethod类型的请求不会做拦截
     * 配置拒接访问的接口，用于严格的线上环境，如需要配置拦截swagger与actuator监控等："/v2/api-docs,/actuator/**"
     */
    private Boolean onlyHandlerMethod = false;

    /**
     * 仅对特定接口进行拦截，输入包名前缀，多个以逗号分开，例如"com.eairlv.utils,com.eairlv.cli"
     * 如果不配置默认会拦截系统会自带的controller的接口，如ApiResourceController、BasicErrorController中的接口，尽管这些接口其作用不大；
     * 如果配置，仅会对配置的包下面的接口进行拦截并生效
     */
    private String onlyPackages = "";

    /**
     * 内外部接口拦截开关
     * 是否区分内外部微服务，因为网关作为微服务中的一员，可直接通过微服务名进行接口访问，因此在客户端进行内外部接口区分并通过访问来源判断是否允许访问
     */
    private Boolean insideOrOutside = true;

    /**
     * 鉴权拦截开关
     * 是否调用请求鉴权，确定此用户是否能够访问该请求
     */
    private Boolean authentication = true;

    /**
     * 鉴权拦截全路径
     */
    private String authenticationUrl = "http://highlight-gov-system-manager/user/check";

    /**
     * 用户信息拦截开关
     * 是否判断携带用户信息，如果未携带，拦截其进行接口访问
     */
    private Boolean user = true;

    /**
     * 用户信息装载方式
     * <thread>通过反射注入{@link ThreadLocal}，避免反射损耗
     * <header>通过反射注入header，保证与内服微服务传递用户信息的方式统一
     * 默认仅配置为header才会使用header方式，如果配置其他字符，默认使用<thread>
     *
     */
    private String userPayload = "thread";


    /**
     * 调试模式
     * 如果开启调试模式，当请求被拦截时，打印详细的请求头信息
     *
     */
    private Boolean debug = false;

}
